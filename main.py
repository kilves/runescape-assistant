import json
import urllib.request
import time
import re
from aiotg import Bot, Chat
import sqlite3

TOPLIST_REQUEST_DELAY = 0.25

with open("token.txt") as f:
    data = f.read()
    bot = Bot(json.loads(data)["token"])

connection = sqlite3.connect("bot.db")
connection.row_factory = sqlite3.Row
c = connection.cursor()

c.execute("CREATE TABLE IF NOT EXISTS monsters(id INTEGER PRIMARY KEY AUTOINCREMENT, "
          "name TEXT, "
          "attack INTEGER, strength INTEGER, defense INTEGER, magic INTEGER, range INTEGER, "
          "a_stab INTEGER, a_slash INTEGER, a_crush INTEGER, a_magic INTEGER, a_range INTEGER, "
          "d_stab INTEGER, d_slash INTEGER, d_crush INTEGER, d_magic INTEGER, d_range INTEGER, "
          "bonus_attack INTEGER, bonus_strength INTEGER, bonus_range_strength INTEGER, bonus_mage_strength INTEGER, "
          "immune_poison INTEGER, immune_venom INTEGER, hp INTEGER)")
c.execute("CREATE TABLE IF NOT EXISTS users("
          "id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, chat_id INTEGER, nick TEXT)")
c.execute("CREATE TABLE IF NOT EXISTS skills(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, 'index' INTEGER)")
connection.commit()


def send_monster(chat: Chat, mon, monsters):
    msg = mon["name"] + ":\n"
    msg += "*Combat*\n\u2665{0}, \u2694{1}, \u270A{2}, \U0001f6e1{3}, \u2728{4}, \U0001f3f9{5}".format(
        mon["hp"], mon["attack"], mon["strength"], mon["defense"], mon["magic"], mon["range"]
    )

    msg += "\n*Attack*\n\U0001f5e1{0}, \U0001f52a{1}, \U0001f528{2}, \u2728{3}, \U0001f3f9{4}".format(
        mon["a_stab"], mon["a_slash"], mon["a_crush"], mon["a_magic"], mon["a_range"]
    )

    msg += "\n*Defense*\n\U0001f5e1{0}, \U0001f52a{1}, \U0001f528{2}, \u2728{3}, \U0001f3f9{4}".format(
        mon["d_stab"], mon["d_slash"], mon["d_crush"], mon["d_magic"], mon["d_range"]
    )

    msg += "\n*Bonuses*\n\u2694{0}, \u270A{1}, \U0001f3f9\u270A{2}, \u2728\u270A{3}%".format(
        mon["bonus_attack"], mon["bonus_strength"], mon["bonus_range_strength"], mon["bonus_mage_strength"]
    )
    msg += "\n"
    if mon["immune_poison"] > 0:
        msg += "\nPoison immune"
    if mon["immune_venom"] > 0:
        msg += "\nVenom immune"

    chat.reply(msg, parse_mode="Markdown")


def send_monsters(chat: Chat, monsters):
    if len(monsters) == 0:
        chat.reply("No monsters found.")
        return

    msg = "Did you mean:\n\n"
    for possible_monster in monsters:
        msg += possible_monster["name"] + "\n"

    chat.reply(msg)


@bot.command("r/help")
def help(chat: Chat):
    chat.reply(text="Commands:\n`/monster searchterm - finds an osrs monster`", parse_mode="Markdown")


@bot.command(r"/dps (.+)")
def dps(chat: Chat, match):
    args = match.group(0).split(" ")[1:]
    print(args)


@bot.command("/nick remove")
def osnick_remove(chat: Chat, match):
    user_id = chat.sender["id"]
    c.execute("DELETE FROM users WHERE user_id=?", user_id)
    connection.commit()
    chat.reply("Nick removed")


@bot.command("/nick add (.+)")
def osnick(chat: Chat, match):
    nick = match.group(1)
    user_id = chat.sender["id"]
    chat_id = chat.id

    if not re.fullmatch(r"[\w\-\s]+", nick):
        return chat.reply("Invalid username")

    c.execute("SELECT * FROM users WHERE user_id!=? AND chat_id=? AND nick=?", (user_id, chat_id, nick))
    if c.fetchone():
        return chat.reply("Username owned by someone else")

    c.execute("SELECT * FROM users WHERE user_id=? AND chat_id=?", (user_id, chat_id))
    if not c.fetchone():
        c.execute("INSERT INTO users(user_id, chat_id, nick) VALUES(?, ?, ?)", (user_id, chat_id, nick))
        chat.send_text(f"Display name \"{nick}\" added")
    else:
        c.execute("UPDATE users SET nick=? WHERE user_id=? AND chat_id=?", (nick, user_id, chat_id))
        chat.send_text(f"Display name changed to \"{nick}\"")
    connection.commit()


def top(chat: Chat, skillname):
    c.execute("SELECT * FROM users WHERE chat_id=?", (chat.id,))
    users = [dict(user) for user in c.fetchall()]
    hiscores = [{"nick": user["nick"], "score": 0} for user in users]

    c.execute("SELECT * FROM skills WHERE name LIKE ?", ("%"+skillname+"%",))
    skill = c.fetchone()
    if not skill:
        return chat.reply("Skill not found")
    index = skill["index"]

    for score in hiscores:
        url = f"https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player={score['nick']}"
        with urllib.request.urlopen(url) as response:
            content = response.read().decode("utf-8")
            xp = int(content.split("\n")[index].split(",")[2])
            score["score"] = xp
        time.sleep(TOPLIST_REQUEST_DELAY)

    hiscores.sort(key=lambda s: s["score"], reverse=True)

    score_text = "\n".join([f"{i+1}: {score['nick']} ({score['score']} xp)" for i, score in enumerate(hiscores)])

    if score_text == "":
        return chat.send_text("No nicks added yet")

    chat.reply(f"`{score_text}`", parse_mode="Markdown")

@bot.command("/top (.+)")
def top_skill(chat: Chat, match):
    skillname = match.group(1)
    top(chat, skillname)

@bot.command("/top")
def top_overall(chat: Chat, match):
    top(chat, "overall")


@bot.command(r"/monster (.+)")
def monster(chat: Chat, match):
    args = match.group(0).split(" ")[1:]
    if len(args) < 1:
        chat.reply(text="Usage: `/monster searchterm`", parse_mode="Markdown")

    search_term = " ".join(args).upper()
    c.execute("SELECT * FROM monsters WHERE UPPER(name) LIKE ?", ("%"+search_term+"%",))
    monsters = [dict(row) for row in c.fetchall()]
    for monster in monsters:
        if monster["name"].upper() == search_term:
            send_monster(chat, monster, monsters)
            return

    send_monsters(chat, monsters)


bot.run()
