# Runescape Assistant

Runescape assistant is a telegram bot that does some of old school runescape related stuff.

## Features
### Top users in a skill
You can create chat-specific top lists to track your players' progress in certain skills.
Just ask players in your chat to add themselves on top list. Once that's done, you can ask
the bot to show a list of your chat's most high level players!

**Only one display name is allowed per telegram user.**

**Note that due to limitations in OSRS API, it's not recommended to add more than ~10 users
to toplist.**

By default, each new user slows down the `/top` command by 0.25 seconds, but this is adjustable with
`TOPLIST_REQUEST_DELAY`. Be aware that lower numbers may result in OSRS API rejecting some
of your requests, breaking the `/top` command altogether.
#### Commands
* `/nick add <nick>` - Add your player's OSRS display to top list
* `/nick remove` - Remove your player's nick to top list
* `/top [skill]` - Show the top list of a skill, or total xp if skill is not provided.

### Monster data
#### Commands
* `/monster <monster>` - Show an OSRS monster's stats

## Requirements
Python 3.6+ and package `aiotg` are required to run this bot.



## Setup
First, create a file called `token.txt` in the same folder as `main.py` and just put your bot's
token (received from [BotFather](https://telegram.me/BotFather)) in it.
place nothing else in `token.txt`.

As of now, you'll need to manually populate your `bot.db` sqlite3 file with data if you want
to host this bot. Import `monsters.csv` to databse to get monster search working and
manually fill `skills` table with desired values, `name` column being the skill's name used
for search and `index` being its index in OSRS [high score list](https://secure.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=Lynx%A0Titan)
starting from 1 - Attack, 2 - Defense etc. This may change in the future if any sort of
migration system is implemented.
